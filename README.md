# Description App
```
Aplicación realizada con la finalidad de copiar en el portapapeles el código de color Hexadecimal 
```

# Tecnologías 
```
JavaScript
Vue.js
Framework diseño Vuetify
```
# app-color

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

# Url Producción 
https://app-color-richard-hernandez.now.sh/

### Url para Clonar
https://gitlab.com/richar33333/app-color-richard-hernandez.git

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


